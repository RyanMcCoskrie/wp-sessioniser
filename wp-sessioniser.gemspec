Gem::Specification.new do |s|
	s.name		= 'wp-sessioniser'
	s.version	= '0.2.1'
	s.executables 	<< 'wp-sessioniser'
	s.date		= '2016-03-27'
	s.summary	= 'Wordpress session analyser'
	s.description	= 'Displays reader sessions from an apache wordpress log'
	s.authors	= ['Ryan McCoskrie']
	s.email		= 'work@ryanmccoskrie.me'
	s.files		= ['bin/wp-sessioniser', 'lib/wp-sessioniser.rb', 'lib/wp-sessioniser/Visitor.rb']
	s.homepage	= 'http://rubygems.org/gems/wp-sessioniser'
	s.license	= 'MIT'
	s.add_runtime_dependency 'apachelogregex', '~> 0.1'
end

