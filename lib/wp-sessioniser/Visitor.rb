#
# = Visitor
#
# Used to handle Visitor profiles in WPSessioniser
#
# Category::
# Package:: WPSessioniser
# Author:: Ryan McCoskrie <ryan.mccoskrie@gmail.com>
# License:: MIT License
#

class Visitor
	attr_reader :hits
	
	def initialize
		@hits = Hash.new
	end

	def push_hit(page, date)
		if @hits.has_key?(page)
			@hits[page].push(date)
		else
			@hits[page] = [date]
		end
	end
end

