#
# = WP Sessioniser
#
# Convert access logs from wordpress blogs into tidy user sessions
#
# Category::
# Package:: WPSessioniser
# Author:: Ryan McCoskrie <ryan.mccoskrie@gmail.com>
# License:: MIT License

require 'apache_log_regex'
require 'wp-sessioniser/Visitor'


#
# = WPSessioniser
#
# == Example Usage
#
#	file = File.open("/var/log/apache/access.log")
#	visitors = WPSessioniser.parse(file)
#

module WPSessioniser
	#Changing this string breaks parsing. I don't know why.
	FORMAT = '%h %l %u %t \"%r\" %>s %b \"{Referer}i\" \"%{User-Agent}i\"'
	PARSER = ApacheLogRegex.new(FORMAT)

	def WPSessioniser.parse file
		visitors = Hash.new()

		file.each do |line|
			h = PARSER.parse! line
			#Ignore non-pages
			next if h["%r"] !~ /GET/
			next if h["%r"] =~ /xmlrpc.php/
			next if h["%r"] =~ /favicon.ico/
			next if h["%r"] =~ /robots.txt/
			next if h["%r"] =~ /\?sccss/
			next if h["%r"] =~ /wp-admin/
			next if h["%r"] =~ /wp-content/
			next if h["%r"] =~ /wp-includes/
			next if h["%r"] =~ /wp-json/
			next if h["%r"] =~ /\/\/images/
			next if h["%r"] =~ /\?plugin=/
			next if h["%r"] =~ /sitemap/
			next if h["%r"] =~ /transformers.txt/

			page = h["%r"].delete('GET ', '').delete(' HTTP/1.1', '')

			if visitors.has_key? h["%h"]
				visitors[h["%h"]].push_hit page, h["%t"]
			else
				visitors[h["%h"]] = Visitor.new
				visitors[h["%h"]].push_hit page, h["%t"]
			end
		end

		return visitors
	end
end

